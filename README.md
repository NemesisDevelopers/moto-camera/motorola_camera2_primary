# Motorola Moto Camera: List 1

<center><img src="https://telegra.ph/file/cf43da1a6481e7c094ff6.jpg"/></center>

Compatible Devices:

- Moto G⁶ Plus (evert)
- Moto X⁴ (payton)
- Moto X⁴ Android One (payton)
- Moto Z² Force Edition (nash)
- Moto Z 2018 (nash)
- Moto Z³ Play (beckham)
- Moto Z³ (messi)
- Motorola One (deen)
- Motorola One Power (chef)
- Motorola P30 (robusta)
- Motorola P30 Note (chef)
- Motorola P30 Play (deen)
- Motorola Z³ (messi)


**How to add it in your tree**

To clone:

`git clone https://gitlab.com/NemesisDevelopers/moto-camera/motorola_camera2_primary.git -b eleven-arm64 packages/apps/MotCamera2`

`git clone https://gitlab.com/NemesisDevelopers/moto-camera/motorola_camera2_overlay.git -b ten packages/apps/MotCamera2-overlay`

`git clone https://gitlab.com/NemesisDevelopers/motorola/motorola_motosignatureapp.git -b eleven packages/apps/MotoSignatureApp`

Add this in your dependencies:

```
 {
   "repository": "motorola_camera2_primary",
   "target_path": "packages/apps/MotCamera2",
   "branch": "eleven-arm64",
   "remote": "moto-camera"
 }
```
Add this in your device.mk or common.mk:

```
# Moto Camera 2
PRODUCT_PACKAGES += \
    MotCamera2
```

# [Download & info](https://telegra.ph/Moto-Camera-2-List-N1-05-09)


 Copyright © 2020-2021 Nemesis Team
